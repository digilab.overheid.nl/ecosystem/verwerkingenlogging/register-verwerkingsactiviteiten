####################################################################################################
## Builder
####################################################################################################
FROM rust:bookworm AS builder

RUN update-ca-certificates

# Create appuser
ENV USER=appuser
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /app

COPY ./ .

RUN cargo build --release

####################################################################################################
## Final image
####################################################################################################
FROM debian:bookworm

RUN apt-get update && apt install -y libssl-dev && rm -rf /var/lib/apt/lists/*

# Import from builder.
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /app

# Copy our build
COPY --from=builder /app/target/release/register_verwerkingsactiviteiten ./
COPY --from=builder /app/templates ./templates/

# Use an unprivileged user.
USER appuser:appuser

ENTRYPOINT ["/app/register_verwerkingsactiviteiten"]
CMD ["serve"]
