CREATE TABLE IF NOT EXISTS organisations (
    id varchar(36) NOT NULL,
    name varchar(42) NOT NULL,
    oin varchar(20) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ NULL,
    UNIQUE(name, oin)
);
