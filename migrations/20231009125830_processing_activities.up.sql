CREATE TYPE basis AS ENUM (
    'TOESTEMMING',
    'UITVOERING_OVEREENKOMST',
    'WETTELIJKE_VERPLICHTING',
    'BESCHERMING_VITAAL_BELANG',
    'PUBLIEKE_TAAK',
    'GERECHTVAARDIGD_BELANG'
);

CREATE TABLE IF NOT EXISTS processing_activities (
    id varchar(36) NOT NULL,
    name varchar(42) NOT NULL,
    goal varchar(42) NOT NULL,
    basis basis,
    explicated_basis varchar(42) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ NULL,
    PRIMARY KEY (id),
    UNIQUE(name, basis)
);
