use crate::{
    app_error,
    models::processing_activity::{ProcessingActivity, ProcessingActivityCreation},
    repositories::processing_activity::ProcessingActivityRepository,
    types::{AppError, AppErrorCode, AppResult},
    utils::{
        extractors::{ExtractRequestId, Path, Query},
        query::{PaginateResponse, PaginateSort, PaginateSortQuery},
        validation::validate_request_data,
    },
};
use axum::{
    extract::{Extension, Json},
    http::StatusCode,
};
use sqlx::{Pool, Postgres};
use uuid::Uuid;

// Route: POST /api/v1/verwerkings_activiteit
#[instrument(skip(pool))]
pub async fn create(
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
    Json(payload): Json<ProcessingActivityCreation>,
) -> AppResult<Json<ProcessingActivity>> {
    validate_request_data(&payload)?;

    let mut activity = ProcessingActivity::new(payload);
    ProcessingActivityRepository::create(&pool, &mut activity).await?;

    Ok(Json(activity))
}

// Route: GET /api/v1/verwerkings_activiteit
#[instrument(skip(pool))]
pub async fn get_all(
    Query(pagination): Query<PaginateSortQuery>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<Json<PaginateResponse<Vec<ProcessingActivity>>>> {
    let paginate_sort = PaginateSort::from(pagination);
    let entries = ProcessingActivityRepository::get_all(&pool, &paginate_sort).await?;

    Ok(Json(entries))
}

// Route: GET "/api/v1/verwerkings_activiteit/:id"
#[instrument(skip(pool))]
pub async fn get_by_id(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<Json<ProcessingActivity>> {
    let activity = ProcessingActivityRepository::get_by_id(&pool, id.to_string()).await?;
    match activity {
        Some(activity) => Ok(Json(activity)),
        _ => Err(app_error!(
            AppErrorCode::NotFound,
            "processing activity could not be found"
        )),
    }
}

// Route: PUT "/api/v1/verwerkings_activiteit/:id"
#[instrument(skip(pool))]
pub async fn update(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
    Json(payload): Json<ProcessingActivityCreation>,
) -> AppResult<Json<ProcessingActivity>> {
    validate_request_data(&payload)?;

    ProcessingActivityRepository::update(&pool, id.to_string(), &payload).await?;

    let activity = ProcessingActivityRepository::get_by_id(&pool, id.to_string()).await?;
    match activity {
        Some(activity) => Ok(Json(activity)),
        _ => Err(app_error!(
            AppErrorCode::NotFound,
            "processing activity could not be found"
        )),
    }
}

// Route: DELETE "/api/v1/verwerkings_activiteit/:id"
#[instrument(skip(pool))]
pub async fn delete(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<StatusCode> {
    let result = ProcessingActivityRepository::delete(&pool, id.to_string()).await?;
    match result {
        1 => Ok(StatusCode::NO_CONTENT),
        _ => Err(app_error!(
            AppErrorCode::InternalError,
            "processing activity could not be found or has already been deleted"
        )),
    }
}
