use crate::{
    app_error,
    layers::SharedState,
    types::{AppError, AppErrorCode, AppResult},
    TEMPLATES,
};
use axum::{
    extract::State,
    http::{header, HeaderMap},
    response::Html,
    Json,
};
use tera::Context;

// Route: GET "/"
pub async fn rapidoc(State(state): State<SharedState>) -> AppResult<Html<String>> {
    let mut ctx = Context::new();
    ctx.insert("base_path", &state.config.base_path);

    Ok(Html(
        TEMPLATES
            .as_ref()
            .map_err(|err| app_error!(AppErrorCode::InternalError, err, "failed to render UI"))?
            .render("rapidoc.html", &ctx)
            .map_err(|err| app_error!(AppErrorCode::InternalError, err))?,
    ))
}

pub fn openapi_spec(base_path: &str) -> AppResult<String> {
    let mut ctx = Context::new();
    ctx.insert("base_path", &base_path);

    TEMPLATES
        .as_ref()
        .map_err(|err| {
            app_error!(
                AppErrorCode::InternalError,
                err,
                "failed to render OpenAPI schema"
            )
        })?
        .render("openapi.yaml", &ctx)
        .map_err(|err| app_error!(AppErrorCode::InternalError, err))
}

// Route: GET "/api/v1/openapi.yaml"
pub async fn schema_yaml(State(state): State<SharedState>) -> AppResult<(HeaderMap, String)> {
    let mut headers = HeaderMap::new();
    headers.insert(header::CONTENT_TYPE, "application/yaml".parse().unwrap());
    headers.insert(header::ACCESS_CONTROL_ALLOW_ORIGIN, "*".parse().unwrap());
    Ok((headers, openapi_spec(&state.config.base_path)?))
}

// Route: GET "/api/v1/openapi.json"
pub async fn schema_json(
    State(state): State<SharedState>,
) -> AppResult<(HeaderMap, Json<serde_yaml::Value>)> {
    let mut headers = HeaderMap::new();
    headers.insert(header::CONTENT_TYPE, "application/json".parse().unwrap());
    headers.insert(header::ACCESS_CONTROL_ALLOW_ORIGIN, "*".parse().unwrap());
    Ok((
        headers,
        Json(serde_yaml::from_str(&openapi_spec(&state.config.base_path)?).unwrap()),
    ))
}

// Route: GET "/health-check"
pub async fn health_check<'a>() -> &'a str {
    "OK"
}
