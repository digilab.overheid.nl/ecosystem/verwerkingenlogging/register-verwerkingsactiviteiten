use crate::{
    app_error,
    models::organisation::{Organisation, OrganisationCreation},
    repositories::organisation::OrganisationRepository,
    types::{AppError, AppErrorCode, AppResult},
    utils::{
        extractors::{ExtractRequestId, Path, Query},
        query::{PaginateResponse, PaginateSort, PaginateSortQuery},
        validation::validate_request_data,
    },
};
use axum::{
    extract::{Extension, Json},
    http::StatusCode,
};
use sqlx::{Pool, Postgres};
use uuid::Uuid;

// Route: POST /api/v1/organisatie
#[instrument(skip(pool))]
pub async fn create(
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
    Json(payload): Json<OrganisationCreation>,
) -> AppResult<Json<Organisation>> {
    validate_request_data(&payload)?;

    let mut organisation = Organisation::new(payload);
    OrganisationRepository::create(&pool, &mut organisation).await?;

    Ok(Json(organisation))
}

// Route: GET /api/v1/organisatie
#[instrument(skip(pool))]
pub async fn get_all(
    Query(pagination): Query<PaginateSortQuery>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<Json<PaginateResponse<Vec<Organisation>>>> {
    let paginate_sort = PaginateSort::from(pagination);
    let entries = OrganisationRepository::get_all(&pool, &paginate_sort).await?;

    Ok(Json(entries))
}

// Route: GET "/api/v1/organisatie/:id"
#[instrument(skip(pool))]
pub async fn get_by_id(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<Json<Organisation>> {
    let organisation = OrganisationRepository::get_by_id(&pool, id.to_string()).await?;
    match organisation {
        Some(organisation) => Ok(Json(organisation)),
        _ => Err(app_error!(
            AppErrorCode::NotFound,
            "organisation could not be found"
        )),
    }
}

// Route: PUT "/api/v1/organisatie/:id"
#[instrument(skip(pool))]
pub async fn update(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
    Json(payload): Json<OrganisationCreation>,
) -> AppResult<Json<Organisation>> {
    validate_request_data(&payload)?;

    OrganisationRepository::update(&pool, id.to_string(), &payload).await?;

    let organisation = OrganisationRepository::get_by_id(&pool, id.to_string()).await?;
    match organisation {
        Some(organisation) => Ok(Json(organisation)),
        _ => Err(app_error!(
            AppErrorCode::NotFound,
            "organisation could not be found"
        )),
    }
}

// Route: DELETE "/api/v1/organisatie/:id"
#[instrument(skip(pool))]
pub async fn delete(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<StatusCode> {
    let result = OrganisationRepository::delete(&pool, id.to_string()).await?;
    match result {
        1 => Ok(StatusCode::NO_CONTENT),
        _ => Err(app_error!(
            AppErrorCode::InternalError,
            "organisation could not be found or has already been deleted"
        )),
    }
}
