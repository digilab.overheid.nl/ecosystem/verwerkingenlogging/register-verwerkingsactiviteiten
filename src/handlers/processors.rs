use crate::{
    app_error,
    models::processor::{Processor, ProcessorCreation},
    repositories::processor::ProcessorRepository,
    types::{AppError, AppErrorCode, AppResult},
    utils::{
        extractors::{ExtractRequestId, Path, Query},
        query::{PaginateResponse, PaginateSort, PaginateSortQuery},
        validation::validate_request_data,
    },
};
use axum::{
    extract::{Extension, Json},
    http::StatusCode,
};
use sqlx::{Pool, Postgres};
use uuid::Uuid;

// Route: POST /api/v1/verwerker
#[instrument(skip(pool))]
pub async fn create(
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
    Json(payload): Json<ProcessorCreation>,
) -> AppResult<Json<Processor>> {
    validate_request_data(&payload)?;

    let mut processor = Processor::new(payload);
    ProcessorRepository::create(&pool, &mut processor).await?;

    Ok(Json(processor))
}

// Route: GET /api/v1/verwerker
#[instrument(skip(pool))]
pub async fn get_all(
    Query(pagination): Query<PaginateSortQuery>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<Json<PaginateResponse<Vec<Processor>>>> {
    let paginate_sort = PaginateSort::from(pagination);
    let entries = ProcessorRepository::get_all(&pool, &paginate_sort).await?;

    Ok(Json(entries))
}

// Route: GET "/api/v1/verwerker/:id"
#[instrument(skip(pool))]
pub async fn get_by_id(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<Json<Processor>> {
    let processor = ProcessorRepository::get_by_id(&pool, id.to_string()).await?;
    match processor {
        Some(processor) => Ok(Json(processor)),
        _ => Err(app_error!(
            AppErrorCode::NotFound,
            "processor could not be found"
        )),
    }
}

// Route: PUT "/api/v1/processor/:id"
#[instrument(skip(pool))]
pub async fn update(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
    Json(payload): Json<ProcessorCreation>,
) -> AppResult<Json<Processor>> {
    validate_request_data(&payload)?;

    ProcessorRepository::update(&pool, id.to_string(), &payload).await?;

    let processor = ProcessorRepository::get_by_id(&pool, id.to_string()).await?;
    match processor {
        Some(processor) => Ok(Json(processor)),
        _ => Err(app_error!(
            AppErrorCode::NotFound,
            "processor could not be found"
        )),
    }
}

// Route: DELETE "/api/v1/processor/:id"
#[instrument(skip(pool))]
pub async fn delete(
    Path(id): Path<Uuid>,
    Extension(pool): Extension<Pool<Postgres>>,
    ExtractRequestId(request_id): ExtractRequestId,
) -> AppResult<StatusCode> {
    let result = ProcessorRepository::delete(&pool, id.to_string()).await?;
    match result {
        1 => Ok(StatusCode::NO_CONTENT),
        _ => Err(app_error!(
            AppErrorCode::InternalError,
            "processor could not be found or has already been deleted"
        )),
    }
}
