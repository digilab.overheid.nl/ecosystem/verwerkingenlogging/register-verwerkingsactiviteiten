use crate::{handlers, layers::SharedState};
use axum::{
    routing::{delete, get, post, put},
    Router,
};

pub fn web(state: SharedState) -> Router<SharedState> {
    Router::new()
        .route("/", get(handlers::web::rapidoc))
        .route("/health-check", get(handlers::web::health_check))
        .route("/api/v1/openapi.yaml", get(handlers::web::schema_yaml))
        .route("/api/v1/openapi.json", get(handlers::web::schema_json))
        .with_state(state)
}

pub mod processing_activities {
    use super::*;

    pub fn api() -> Router<SharedState> {
        Router::new()
            .route("/", post(handlers::processing_activities::create))
            .route("/", get(handlers::processing_activities::get_all))
            .route("/:id", get(handlers::processing_activities::get_by_id))
            .route("/:id", put(handlers::processing_activities::update))
            .route("/:id", delete(handlers::processing_activities::delete))
    }
}

pub mod organisations {
    use super::*;

    pub fn api() -> Router<SharedState> {
        Router::new()
            .route("/", post(handlers::organisations::create))
            .route("/", get(handlers::organisations::get_all))
            .route("/:id", get(handlers::organisations::get_by_id))
            .route("/:id", put(handlers::organisations::update))
            .route("/:id", delete(handlers::organisations::delete))
    }
}

pub mod processors {
    use super::*;

    pub fn api() -> Router<SharedState> {
        Router::new()
            .route("/", post(handlers::processors::create))
            .route("/", get(handlers::processors::get_all))
            .route("/:id", get(handlers::processors::get_by_id))
            .route("/:id", put(handlers::processors::update))
            .route("/:id", delete(handlers::processors::delete))
    }
}
