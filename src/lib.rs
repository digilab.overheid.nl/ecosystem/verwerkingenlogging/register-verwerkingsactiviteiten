pub mod config;
mod handlers;
pub mod layers;
pub mod models;
pub mod repositories;
pub mod routes;
mod server;
mod types;
mod utils;

#[macro_use]
extern crate tracing;

use tera::Tera;

#[macro_use]
extern crate lazy_static;

lazy_static! {
    pub static ref TEMPLATES: Result<Tera, tera::Error> = {
        let mut tera = Tera::new("templates/**/*")?;
        tera.autoescape_on(vec![".yml", ".html"]);
        Ok(tera)
    };
}

pub const APP_NAME: &str = "Register Verwerkingslogging";

pub use server::start_server;
pub use types::{AppError, AppErrorCode, AppErrorMessage, AppResult, CliError, CliResult};
