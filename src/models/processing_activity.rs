use serde::{Deserialize, Serialize};
use sqlx::types::chrono::{DateTime, Utc};
use uuid::Uuid;
use validator::Validate;

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize, sqlx::Type)]
#[sqlx(type_name = "basis", rename_all = "SCREAMING_SNAKE_CASE")]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Basis {
    Toestemming,
    UitvoeringOvereenkomst,
    WettelijkeVerplichting,
    BeschermingVitaalBelang,
    PubliekeTaak,
    GerechtvaardigdBelang,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProcessingActivity {
    pub id: String,
    #[serde(rename = "naam")]
    pub name: String,
    #[serde(rename = "doel")]
    pub goal: String,
    #[serde(rename = "grondslag")]
    pub basis: Basis,
    #[serde(rename = "toelichting_grondslag")]
    pub explicated_basis: String,
    #[serde(rename = "aangemaakt_op")]
    pub created_at: DateTime<Utc>,
    #[serde(rename = "aangepast_op")]
    pub updated_at: Option<DateTime<Utc>>,
}

impl ProcessingActivity {
    pub fn new(activity: ProcessingActivityCreation) -> Self {
        Self {
            id: Uuid::new_v4().to_string(),
            name: activity.name,
            goal: activity.goal,
            basis: activity.basis,
            explicated_basis: activity.explicated_basis,
            created_at: Utc::now(),
            updated_at: None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Validate)]
pub struct ProcessingActivityCreation {
    #[serde(rename = "naam")]
    pub name: String,
    #[serde(rename = "doel")]
    pub goal: String,
    #[serde(rename = "grondslag")]
    pub basis: Basis,
    #[serde(rename = "toelichting_grondslag")]
    pub explicated_basis: String,
}
