use serde::{Deserialize, Serialize};
use sqlx::types::chrono::{DateTime, Utc};
use uuid::Uuid;
use validator::Validate;

use crate::utils::validation::OIN;

#[derive(Serialize, Deserialize, Debug)]
pub struct Processor {
    pub id: String,
    pub oin: String,
    #[serde(rename = "naam")]
    pub name: String,
    #[serde(rename = "aangemaakt_op")]
    pub created_at: DateTime<Utc>,
    #[serde(rename = "aangepast_op")]
    pub updated_at: Option<DateTime<Utc>>,
}

impl Processor {
    pub fn new(processor: ProcessorCreation) -> Self {
        Self {
            id: Uuid::new_v4().to_string(),
            oin: processor.oin,
            name: processor.name,
            created_at: Utc::now(),
            updated_at: None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Validate)]
pub struct ProcessorCreation {
    #[validate(regex(path = "OIN", message = "invalid format"))]
    pub oin: String,
    #[serde(rename = "naam")]
    pub name: String,
}
