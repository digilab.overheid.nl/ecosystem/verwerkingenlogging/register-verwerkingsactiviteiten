use crate::{
    models::organisation::{Organisation, OrganisationCreation},
    types::AppResult,
    utils::query::{PaginateResponse, PaginateSort},
};
use chrono::Utc;
use futures::TryStreamExt;
use sqlx::{PgPool, Row};

pub struct OrganisationRepository;

impl OrganisationRepository {
    /// Add a new organisation
    #[tracing::instrument(skip(pool))]
    pub async fn create(pool: &PgPool, organisation: &mut Organisation) -> AppResult<()> {
        sqlx::query!(
            r#"
                INSERT INTO organisations (id, oin, name, created_at)
                VALUES ( $1, $2, $3, $4 )
            "#,
            organisation.id,
            organisation.oin,
            organisation.name,
            organisation.created_at,
        )
        .execute(pool)
        .await?;

        Ok(())
    }

    /// Returns all organisations
    #[instrument(skip(pool))]
    pub async fn get_all<'a>(
        pool: &'a PgPool,
        paginate_sort: &'a PaginateSort,
    ) -> AppResult<PaginateResponse<Vec<Organisation>>> {
        let total = Self::get_total(pool).await?;

        let mut query = String::from(
            "
            SELECT id, oin, name, created_at, updated_at
            FROM organisations
            ",
        );

        // Sorts and pagination
        query.push_str(&paginate_sort.get_sorts_sql(Some(&[
            "id",
            "oin",
            "name",
            "created_at",
            "updated_at",
        ])));
        query.push_str(&paginate_sort.get_pagination_sql());

        let mut rows = sqlx::query(&query)
            .bind(i32::try_from(paginate_sort.limit)?)
            .bind(i32::try_from(paginate_sort.offset)?)
            .fetch(pool);

        let mut organisations = vec![];
        while let Some(row) = rows.try_next().await? {
            organisations.push(Organisation {
                id: row.try_get("id")?,
                oin: row.try_get("oin")?,
                name: row.try_get("name")?,
                created_at: row.try_get("created_at")?,
                updated_at: row.try_get("updated_at")?,
            });
        }
        Ok(PaginateResponse {
            data: organisations,
            total,
        })
    }

    /// Returns an organisation by its ID
    #[instrument(skip(pool))]
    pub async fn get_by_id(pool: &PgPool, id: String) -> AppResult<Option<Organisation>> {
        let result = sqlx::query!(
            r#"
                SELECT id, oin, name, created_at, updated_at
                FROM organisations
                WHERE id = $1
            "#,
            id
        )
        .fetch_optional(pool)
        .await?;

        match result {
            Some(result) => Ok(Some(Organisation {
                id: result.id,
                oin: result.oin,
                name: result.name,
                created_at: result.created_at,
                updated_at: result.updated_at,
            })),
            None => Ok(None),
        }
    }

    /// Delete an organisation
    #[instrument(skip(pool))]
    pub async fn delete(pool: &PgPool, id: String) -> AppResult<u64> {
        let result = sqlx::query!(
            r#"
                DELETE FROM organisations
                WHERE id = $1
            "#,
            id
        )
        .execute(pool)
        .await?;

        Ok(result.rows_affected())
    }

    /// Update an organisation
    #[instrument(skip(pool))]
    pub async fn update(
        pool: &PgPool,
        id: String,
        organisation: &OrganisationCreation,
    ) -> AppResult<()> {
        sqlx::query!(
            r#"
                UPDATE organisations
                SET oin = $1, name = $2, updated_at = $3
                WHERE id = $4
            "#,
            organisation.oin,
            organisation.name,
            Some(Utc::now()),
            id
        )
        .execute(pool)
        .await?;

        Ok(())
    }

    /// Get amount of existing organisations
    #[instrument(skip(pool))]
    async fn get_total(pool: &PgPool) -> Result<i64, sqlx::Error> {
        let query = r#"
            SELECT COUNT(id) AS n
            FROM organisations
        "#;

        Ok(sqlx::query(query).fetch_one(pool).await?.get("n"))
    }
}
