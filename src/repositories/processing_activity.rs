use crate::{
    models::processing_activity::{Basis, ProcessingActivity, ProcessingActivityCreation},
    types::AppResult,
    utils::query::{PaginateResponse, PaginateSort},
};
use chrono::Utc;
use futures::TryStreamExt;
use sqlx::{PgPool, Row};

pub struct ProcessingActivityRepository;

impl ProcessingActivityRepository {
    /// Add a new processing activity
    #[tracing::instrument(skip(pool))]
    pub async fn create(pool: &PgPool, activity: &mut ProcessingActivity) -> AppResult<()> {
        sqlx::query!(
            r#"
                INSERT INTO processing_activities (id, name, goal, basis, explicated_basis, created_at)
                VALUES ( $1, $2, $3, $4, $5, $6 )
            "#,
            activity.id,
            activity.name,
            activity.goal,
            activity.basis as Basis,
            activity.explicated_basis,
            activity.created_at,
        )
        .execute(pool)
        .await?;

        Ok(())
    }

    /// Returns all activities
    #[instrument(skip(pool))]
    pub async fn get_all<'a>(
        pool: &'a PgPool,
        paginate_sort: &'a PaginateSort,
    ) -> AppResult<PaginateResponse<Vec<ProcessingActivity>>> {
        let total = Self::get_total(pool).await?;

        let mut query = String::from(
            "
            SELECT id, name, goal, basis, explicated_basis, created_at, updated_at
            FROM processing_activities
            ",
        );

        // Sorts and pagination
        query.push_str(&paginate_sort.get_sorts_sql(Some(&[
            "id",
            "name",
            "basis",
            "explicated_basis",
            "created_at",
            "updated_at",
        ])));
        query.push_str(&paginate_sort.get_pagination_sql());

        let mut rows = sqlx::query(&query)
            .bind(i32::try_from(paginate_sort.limit)?)
            .bind(i32::try_from(paginate_sort.offset)?)
            .fetch(pool);

        let mut activities = vec![];
        while let Some(row) = rows.try_next().await? {
            activities.push(ProcessingActivity {
                id: row.try_get("id")?,
                name: row.try_get("name")?,
                goal: row.try_get("goal")?,
                basis: row.try_get("basis")?,
                explicated_basis: row.try_get("explicated_basis")?,
                created_at: row.try_get("created_at")?,
                updated_at: row.try_get("updated_at")?,
            });
        }
        Ok(PaginateResponse {
            data: activities,
            total,
        })
    }

    /// Returns a processing activity by its ID
    #[instrument(skip(pool))]
    pub async fn get_by_id(pool: &PgPool, id: String) -> AppResult<Option<ProcessingActivity>> {
        let result = sqlx::query!(
            r#"
                SELECT id, name, goal, basis as "basis: Basis", explicated_basis, created_at, updated_at
                FROM processing_activities
                WHERE id = $1
            "#,
            id
        )
        .fetch_optional(pool)
        .await?;

        match result {
            Some(result) => Ok(Some(ProcessingActivity {
                id: result.id,
                name: result.name,
                goal: result.goal,
                basis: result.basis.unwrap(),
                explicated_basis: result.explicated_basis,
                created_at: result.created_at,
                updated_at: result.updated_at,
            })),
            None => Ok(None),
        }
    }

    /// Delete a processing activity
    #[instrument(skip(pool))]
    pub async fn delete(pool: &PgPool, id: String) -> AppResult<u64> {
        let result = sqlx::query!(
            r#"
                DELETE FROM processing_activities
                WHERE id = $1
            "#,
            id
        )
        .execute(pool)
        .await?;

        Ok(result.rows_affected())
    }

    /// Update a processing activity
    #[instrument(skip(pool))]
    pub async fn update(
        pool: &PgPool,
        id: String,
        activity: &ProcessingActivityCreation,
    ) -> AppResult<()> {
        sqlx::query!(
            r#"
                UPDATE processing_activities
                SET name = $1, goal = $2, basis = $3, explicated_basis = $4, updated_at = $5
                WHERE id = $6
            "#,
            activity.name,
            activity.goal,
            activity.basis as Basis,
            activity.explicated_basis,
            Some(Utc::now()),
            id
        )
        .execute(pool)
        .await?;

        Ok(())
    }

    /// Get amount of existing activities
    #[instrument(skip(pool))]
    async fn get_total(pool: &PgPool) -> Result<i64, sqlx::Error> {
        let query = r#"
            SELECT COUNT(id) AS n
            FROM processing_activities
        "#;

        Ok(sqlx::query(query).fetch_one(pool).await?.get("n"))
    }
}
