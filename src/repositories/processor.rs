use crate::{
    models::processor::{Processor, ProcessorCreation},
    types::AppResult,
    utils::query::{PaginateResponse, PaginateSort},
};
use chrono::Utc;
use futures::TryStreamExt;
use sqlx::{PgPool, Row};

pub struct ProcessorRepository;

impl ProcessorRepository {
    /// Add a new processor
    #[tracing::instrument(skip(pool))]
    pub async fn create(pool: &PgPool, processor: &mut Processor) -> AppResult<()> {
        sqlx::query!(
            r#"
                INSERT INTO processors (id, oin, name, created_at)
                VALUES ( $1, $2, $3, $4 )
            "#,
            processor.id,
            processor.oin,
            processor.name,
            processor.created_at,
        )
        .execute(pool)
        .await?;

        Ok(())
    }

    /// Returns all processors
    #[instrument(skip(pool))]
    pub async fn get_all<'a>(
        pool: &'a PgPool,
        paginate_sort: &'a PaginateSort,
    ) -> AppResult<PaginateResponse<Vec<Processor>>> {
        let total = Self::get_total(pool).await?;

        let mut query = String::from(
            "
            SELECT id, oin, name, created_at, updated_at
            FROM processors
            ",
        );

        // Sorts and pagination
        query.push_str(&paginate_sort.get_sorts_sql(Some(&[
            "id",
            "oin",
            "name",
            "created_at",
            "updated_at",
        ])));
        query.push_str(&paginate_sort.get_pagination_sql());

        let mut rows = sqlx::query(&query)
            .bind(i32::try_from(paginate_sort.limit)?)
            .bind(i32::try_from(paginate_sort.offset)?)
            .fetch(pool);

        let mut processors = vec![];
        while let Some(row) = rows.try_next().await? {
            processors.push(Processor {
                id: row.try_get("id")?,
                oin: row.try_get("oin")?,
                name: row.try_get("name")?,
                created_at: row.try_get("created_at")?,
                updated_at: row.try_get("updated_at")?,
            });
        }
        Ok(PaginateResponse {
            data: processors,
            total,
        })
    }

    /// Returns a processor by its ID
    #[instrument(skip(pool))]
    pub async fn get_by_id(pool: &PgPool, id: String) -> AppResult<Option<Processor>> {
        let result = sqlx::query!(
            r#"
                SELECT id, oin, name, created_at, updated_at
                FROM processors
                WHERE id = $1
            "#,
            id
        )
        .fetch_optional(pool)
        .await?;

        match result {
            Some(result) => Ok(Some(Processor {
                id: result.id,
                oin: result.oin,
                name: result.name,
                created_at: result.created_at,
                updated_at: result.updated_at,
            })),
            None => Ok(None),
        }
    }

    /// Delete a processor
    #[instrument(skip(pool))]
    pub async fn delete(pool: &PgPool, id: String) -> AppResult<u64> {
        let result = sqlx::query!(
            r#"
                DELETE FROM processors
                WHERE id = $1
            "#,
            id
        )
        .execute(pool)
        .await?;

        Ok(result.rows_affected())
    }

    /// Update a processor
    #[instrument(skip(pool))]
    pub async fn update(pool: &PgPool, id: String, processor: &ProcessorCreation) -> AppResult<()> {
        sqlx::query!(
            r#"
                UPDATE processors
                SET oin = $1, name = $2, updated_at = $3
                WHERE id = $4
            "#,
            processor.oin,
            processor.name,
            Some(Utc::now()),
            id
        )
        .execute(pool)
        .await?;

        Ok(())
    }

    /// Get amount of existing processors
    #[instrument(skip(pool))]
    async fn get_total(pool: &PgPool) -> Result<i64, sqlx::Error> {
        let query = r#"
            SELECT COUNT(id) AS n
            FROM processors
        "#;

        Ok(sqlx::query(query).fetch_one(pool).await?.get("n"))
    }
}
