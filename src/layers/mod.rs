pub mod logger;
pub mod prometheus;

use crate::{
    app_error,
    config::Config,
    types::{AppError, AppErrorCode},
};
use axum::{
    body::Full,
    http::{HeaderValue, Request},
    middleware::Next,
    response::{IntoResponse, Response},
};
use hyper::{body::to_bytes, StatusCode};
use std::{str::from_utf8, sync::Arc};
use tower_http::request_id::{MakeRequestId, RequestId};
use uuid::Uuid;

// ================ States ================

pub type SharedState = Arc<State>;

pub struct State {
    pub config: ConfigState,
}

impl State {
    pub fn init(config: &Config) -> Self {
        info!("Init app state");
        Self {
            config: config.clone().into(),
        }
    }
}

pub struct ConfigState {
    pub base_path: String,
}

impl From<Config> for ConfigState {
    fn from(config: Config) -> Self {
        Self {
            base_path: config.server_base_path.trim_end_matches('/').to_string(),
        }
    }
}

// ================ Request ID ================

#[derive(Clone, Copy)]
pub struct MakeRequestUuid;

impl MakeRequestId for MakeRequestUuid {
    fn make_request_id<B>(&mut self, _request: &Request<B>) -> Option<RequestId> {
        let id = Uuid::new_v4().to_string().parse();
        match id {
            Ok(id) => Some(RequestId::new(id)),
            _ => None,
        }
    }
}

// =============== Override some HTTP errors ================

/// Layer which overrides a bunch of HTTP errors by using `AppError`
pub async fn override_http_errors<B>(req: Request<B>, next: Next<B>) -> impl IntoResponse {
    let response = next.run(req).await;

    // If it is an image, audio or video, we return response
    let headers = response.headers();
    if let Some(content_type) = headers.get("content-type") {
        let content_type = content_type.to_str().unwrap_or_default();
        if content_type.starts_with("image/")
            || content_type.starts_with("audio/")
            || content_type.starts_with("video/")
        {
            return response;
        }
    }

    let (parts, body) = response.into_parts();
    match to_bytes(body).await {
        Ok(body_bytes) => match String::from_utf8(body_bytes.to_vec()) {
            Ok(body) => match parts.status {
                StatusCode::METHOD_NOT_ALLOWED => {
                    app_error!(AppErrorCode::MethodNotAllowed).into_response()
                }
                StatusCode::UNPROCESSABLE_ENTITY => {
                    if body.contains("Failed to deserialize the JSON body") {
                        app_error!(AppErrorCode::UnprocessableEntity, body).into_response()
                    } else {
                        Response::from_parts(parts, axum::body::boxed(Full::from(body)))
                    }
                }
                _ => Response::from_parts(parts, axum::body::boxed(Full::from(body))),
            },
            Err(err) => app_error!(AppErrorCode::InternalError, err.to_string()).into_response(),
        },
        Err(err) => app_error!(AppErrorCode::InternalError, err.to_string()).into_response(),
    }
}

/// Layer which is responsible for static response header(s)
pub async fn set_response_headers<B>(req: Request<B>, next: Next<B>) -> impl IntoResponse {
    let mut response = next.run(req).await;

    let headers = response.headers_mut();
    headers.insert("API-Version", env!("CARGO_PKG_VERSION").parse().unwrap());

    response
}

// =============== Utils ================

/// Convert `HeaderValue` to `&str`
pub fn header_value_to_str(value: Option<&HeaderValue>) -> &str {
    match value {
        Some(value) => from_utf8(value.as_bytes()).unwrap_or_default(),
        None => "",
    }
}
