use crate::{
    app_error,
    types::{AppError, AppErrorCode, AppResult},
};
use regex::Regex;
use validator::Validate;

lazy_static! {
    pub static ref OIN: Regex = Regex::new(r"^\d{20}$").unwrap();
}

pub fn validate_request_data<T: Validate>(data: &T) -> AppResult<()> {
    match data.validate() {
        Ok(_) => Ok(()),
        Err(errors) => Err(app_error!(
            AppErrorCode::UnprocessableEntity,
            errors.to_string()
        )),
    }
}
