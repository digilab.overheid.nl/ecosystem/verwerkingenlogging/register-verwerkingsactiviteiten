.PHONY: run
run:
	cargo run --bin register_verwerkingsactiviteiten serve

.PHONY: build
build:
	cargo build --release --bin register_verwerkingsactiviteiten

.PHONY: clean
clean:
	cargo clean

# cargo install cargo-watch
.PHONY: watch
watch:
	cargo watch -x 'run --color=always --bin register_verwerkingsactiviteiten serve 2>&1 | less'

.PHONY: test
test:
	cargo test

.PHONY: container
CRI:=$(shell type -p podman || echo docker)
container:
	$(CRI) build --net=host -f Dockerfile -t registry.int.tooling.digilab.network/digilab.overheid.nl/ecosystem/verwerkingenlogging/register-verwerkingsactiviteiten:latest .

.PHONY: check
check: udeps clippy

## cargo install cargo-udeps
.PHONY: udeps
udeps:
	cargo +nightly udeps

.PHONY: clippy
clippy:
	cargo clippy
