use super::helpers::processing_activity::{
    create, delete, fetch_all, fetch_one, update, TestProcessingActivity,
};
use crate::{
    api::helpers::TestPaginateResponse,
    helper::{TestApp, TestAppBuilder},
};
use axum::http::StatusCode;
use rvva::models::processing_activity::Basis;
use uuid::Uuid;

#[tokio::test]
async fn test_api_create_processing_activity() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "naam": "foo",
            "doel": "bar",
            "grondslag": "TOESTEMMING",
            "toelichting_grondslag": "baz",
        })
        .to_string(),
    )
    .await;
    assert_eq!(response.status_code, StatusCode::OK);
}

#[tokio::test]
async fn test_api_create_processing_activity_invalid_basis() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "naam": "foo",
            "doel": "bar",
            "grondslag": "INVALID",
            "toelichting_grondslag": "baz",
        })
        .to_string(),
    )
    .await;
    assert_eq!(response.status_code, StatusCode::UNPROCESSABLE_ENTITY);
}

#[tokio::test]
async fn test_api_create_processing_activity_invalid_json() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(&app, "invalid_json".to_string()).await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_fetch_all_processing_activities() {
    let app: TestApp = TestAppBuilder::new().await.build();

    for i in 0..2 {
        create(
            &app,
            serde_json::json!({
                "naam": format!("foo-{i}"),
                "doel": "bar",
                "grondslag": "UITVOERING_OVEREENKOMST",
                "toelichting_grondslag": "baz",
            })
            .to_string(),
        )
        .await;
    }

    let response = fetch_all(&app, None).await;
    assert_eq!(response.status_code, StatusCode::OK);

    let activities: TestPaginateResponse<Vec<TestProcessingActivity>> =
        serde_json::from_str(&response.body.to_string()).expect("failed to deserialise body");
    assert_eq!(activities.data.len(), 2);
    assert_eq!(activities.total, 2);
}

#[tokio::test]
async fn test_api_fetch_all_processing_activities_invalid_filter() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = fetch_all(&app, Some("p=not_an_int")).await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_fetch_one_processing_activity() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "naam": "foo",
            "doel": "bar",
            "grondslag": "WETTELIJKE_VERPLICHTING",
            "toelichting_grondslag": "baz",
        })
        .to_string(),
    )
    .await;

    let activity_id = TestProcessingActivity::from_body(&response.body.to_string()).id;

    let response = fetch_one(&app, &activity_id).await;
    let body = TestProcessingActivity::from_body(&response.body.to_string());
    assert_eq!(response.status_code, StatusCode::OK);
    assert_eq!(body.id, activity_id);
    assert_eq!(body.updated_at, None);
}

#[tokio::test]
async fn test_api_fetch_one_processing_activity_invalid_id() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = fetch_one(&app, "not_an_uuid").await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_fetch_one_processing_activity_unknown_id() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = fetch_one(&app, &Uuid::new_v4().to_string()).await;
    assert_eq!(response.status_code, StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn test_api_update_processing_activity() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "naam": "foo",
            "doel": "bar",
            "grondslag": "PUBLIEKE_TAAK",
            "toelichting_grondslag": "foobar",
        })
        .to_string(),
    )
    .await;

    let activity_id = TestProcessingActivity::from_body(&response.body.to_string()).id;

    let response = update(
        &app,
        serde_json::json!({
            "naam": "bar",
            "doel": "foo",
            "grondslag": "GERECHTVAARDIGD_BELANG",
            "toelichting_grondslag": "barfoo",
        })
        .to_string(),
        &activity_id,
    )
    .await;
    assert_eq!(response.status_code, StatusCode::OK);

    let activity = TestProcessingActivity::from_body(&response.body.to_string());
    assert_eq!(activity.name, String::from("bar"));
    assert_eq!(activity.goal, String::from("foo"));
    assert_eq!(activity.basis, Basis::GerechtvaardigdBelang);
    assert_eq!(activity.explicated_basis, String::from("barfoo"));
    assert_ne!(activity.updated_at, None)
}

#[tokio::test]
async fn test_api_update_processing_activity_invalid_basis() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "naam": "foo",
            "doel": "bar",
            "grondslag": "PUBLIEKE_TAAK",
            "toelichting_grondslag": "foobar",
        })
        .to_string(),
    )
    .await;

    let activity_id = TestProcessingActivity::from_body(&response.body.to_string()).id;

    let response = update(
        &app,
        serde_json::json!({
            "naam": "bar",
            "doel": "foo",
            "grondslag": "INVALID",
            "toelichting_grondslag": "barfoo",
        })
        .to_string(),
        &activity_id,
    )
    .await;
    assert_eq!(response.status_code, StatusCode::UNPROCESSABLE_ENTITY);
}

#[tokio::test]
async fn test_api_update_processing_activity_unknown_id() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = update(
        &app,
        serde_json::json!({
            "naam": "bar",
            "doel": "foo",
            "grondslag": "PUBLIEKE_TAAK",
            "toelichting_grondslag": "barfoo",
        })
        .to_string(),
        &Uuid::new_v4().to_string(),
    )
    .await;
    assert_eq!(response.status_code, StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn test_api_delete_processing_activity() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "naam": "foo",
            "doel": "bar",
            "grondslag": "TOESTEMMING",
            "toelichting_grondslag": "baz",
        })
        .to_string(),
    )
    .await;

    let activity_id = TestProcessingActivity::from_body(&response.body.to_string()).id;

    let response = delete(&app, &activity_id).await;
    assert_eq!(response.status_code, StatusCode::NO_CONTENT);
}

#[tokio::test]
async fn test_api_delete_processing_activity_invalid_id() {
    let app: TestApp = TestAppBuilder::new().await.build();
    let response = delete(&app, "not_an_uuid").await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_delete_processing_activity_unknown_id() {
    let app: TestApp = TestAppBuilder::new().await.build();
    let response = delete(&app, &Uuid::new_v4().to_string()).await;
    assert_eq!(response.status_code, StatusCode::INTERNAL_SERVER_ERROR);
}
