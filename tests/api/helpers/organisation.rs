//! Helpers for organisation API tests

use super::TestResponse;
use crate::helper::TestApp;
use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct TestOrganisation {
    pub id: String,
    pub oin: String,
    #[serde(rename = "naam")]
    pub name: String,
    #[serde(rename = "aangemaakt_op")]
    pub created_at: DateTime<Utc>,
    #[serde(rename = "aangepast_op")]
    pub updated_at: Option<DateTime<Utc>>,
}

impl TestOrganisation {
    pub fn from_body(body: &str) -> Self {
        serde_json::from_str(body).expect("an error occurred when deserialising organisation body")
    }
}

/// Organisation creation request helper
pub async fn create(app: &TestApp, body: String) -> TestResponse {
    TestResponse::new(app, "/api/v1/organisatie", "POST", Some(body)).await
}

/// Return all organisations
pub async fn fetch_all(app: &TestApp, params: Option<&str>) -> TestResponse {
    TestResponse::new(
        app,
        &format!("/api/v1/organisatie?{}", params.unwrap_or_default(),),
        "GET",
        None,
    )
    .await
}

/// Return an organisation
pub async fn fetch_one(app: &TestApp, id: &str) -> TestResponse {
    TestResponse::new(app, &format!("/api/v1/organisatie/{id}"), "GET", None).await
}

/// Update an organisation
pub async fn update(app: &TestApp, body: String, id: &str) -> TestResponse {
    TestResponse::new(app, &format!("/api/v1/organisatie/{id}"), "PUT", Some(body)).await
}

/// Delete an organisation
pub async fn delete(app: &TestApp, id: &str) -> TestResponse {
    TestResponse::new(app, &format!("/api/v1/organisatie/{id}"), "DELETE", None).await
}
