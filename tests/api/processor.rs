use super::helpers::processor::{create, delete, fetch_all, fetch_one, update, TestProcessor};
use crate::{
    api::helpers::TestPaginateResponse,
    helper::{TestApp, TestAppBuilder},
};
use axum::http::StatusCode;
use uuid::Uuid;

#[tokio::test]
async fn test_api_create_processor() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "oin": "01234567891011121314",
            "naam": "foo",
        })
        .to_string(),
    )
    .await;
    assert_eq!(response.status_code, StatusCode::OK);
}

#[tokio::test]
async fn test_api_create_processor_invalid_oin() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "oin": "invalid_oin",
            "naam": "foo",
        })
        .to_string(),
    )
    .await;
    assert_eq!(response.status_code, StatusCode::UNPROCESSABLE_ENTITY);
}

#[tokio::test]
async fn test_api_create_processor_invalid_json() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(&app, "invalid_json".to_string()).await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_fetch_all_processors() {
    let app: TestApp = TestAppBuilder::new().await.build();

    for i in 0..2 {
        create(
            &app,
            serde_json::json!({
                "oin": format!("0123456789101112131{i}"),
                "naam": format!("foo-{i}"),
            })
            .to_string(),
        )
        .await;
    }

    let response = fetch_all(&app, None).await;
    assert_eq!(response.status_code, StatusCode::OK);

    let processors: TestPaginateResponse<Vec<TestProcessor>> =
        serde_json::from_str(&response.body.to_string()).expect("failed to deserialise body");
    assert_eq!(processors.data.len(), 2);
    assert_eq!(processors.total, 2);
}

#[tokio::test]
async fn test_api_fetch_all_processors_invalid_filter() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = fetch_all(&app, Some("p=not_an_int")).await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_fetch_one_processor() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "oin": "01234567891011121314",
            "naam": "foo",
        })
        .to_string(),
    )
    .await;

    let processor_id = TestProcessor::from_body(&response.body.to_string()).id;

    let response = fetch_one(&app, &processor_id).await;
    let body = TestProcessor::from_body(&response.body.to_string());
    assert_eq!(response.status_code, StatusCode::OK);
    assert_eq!(body.id, processor_id);
    assert_eq!(body.updated_at, None);
}

#[tokio::test]
async fn test_api_fetch_one_processor_invalid_id() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = fetch_one(&app, "not_an_uuid").await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_fetch_one_processor_unknown_id() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = fetch_one(&app, &Uuid::new_v4().to_string()).await;
    assert_eq!(response.status_code, StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn test_api_update_processor() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "oin": "01234567891011121314",
            "naam": "foo",
        })
        .to_string(),
    )
    .await;

    let processor_id = TestProcessor::from_body(&response.body.to_string()).id;

    let response = update(
        &app,
        serde_json::json!({
            "oin": "00012345678910111213",
            "naam": "bar",
        })
        .to_string(),
        &processor_id,
    )
    .await;
    assert_eq!(response.status_code, StatusCode::OK);

    let processor = TestProcessor::from_body(&response.body.to_string());
    assert_eq!(processor.oin, String::from("00012345678910111213"));
    assert_eq!(processor.name, String::from("bar"));
    assert_ne!(processor.updated_at, None)
}

#[tokio::test]
async fn test_api_update_processor_invalid_oin() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "oin": "01234567891011121314",
            "naam": "foo",
        })
        .to_string(),
    )
    .await;

    let processor_id = TestProcessor::from_body(&response.body.to_string()).id;

    let response = update(
        &app,
        serde_json::json!({
            "oin": "invalid_oin",
            "naam": "bar",
        })
        .to_string(),
        &processor_id,
    )
    .await;
    assert_eq!(response.status_code, StatusCode::UNPROCESSABLE_ENTITY);
}

#[tokio::test]
async fn test_api_update_processor_unknown_id() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = update(
        &app,
        serde_json::json!({
            "oin": "01234567891011121314",
            "naam": "foo",
        })
        .to_string(),
        &Uuid::new_v4().to_string(),
    )
    .await;
    assert_eq!(response.status_code, StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn test_api_delete_processor() {
    let app: TestApp = TestAppBuilder::new().await.build();

    let response = create(
        &app,
        serde_json::json!({
            "oin": "01234567891011121314",
            "naam": "foo",
        })
        .to_string(),
    )
    .await;

    let processor_id = TestProcessor::from_body(&response.body.to_string()).id;

    let response = delete(&app, &processor_id).await;
    assert_eq!(response.status_code, StatusCode::NO_CONTENT);
}

#[tokio::test]
async fn test_api_delete_processor_invalid_id() {
    let app: TestApp = TestAppBuilder::new().await.build();
    let response = delete(&app, "not_an_uuid").await;
    assert_eq!(response.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn test_api_delete_processor_unknown_id() {
    let app: TestApp = TestAppBuilder::new().await.build();
    let response = delete(&app, &Uuid::new_v4().to_string()).await;
    assert_eq!(response.status_code, StatusCode::INTERNAL_SERVER_ERROR);
}
